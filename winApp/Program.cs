﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace winApp {
    class Program {
        static void Main(string[] args) {
            ConfigureService.Configure();
        }
    }

    public class MyService {
        public void Start() {
            // write code here that runs when the Windows Service starts up.  
            //Task.Run(() => Execute());
            var taskMain = Execute();
            var task1 = ExecuteSimpleJob("echoSample1.bat", 10);
            var task2 = ExecuteSimpleJob("echoSample2.bat", 20);
            var task3 = ExecuteSimpleJob("echoSample3.bat", 30);
            Task.WaitAll(new[] { taskMain, task1, task2, task3 });
        }
        public void Stop() {
            // write code here that runs when the Windows Service stops.  
            Environment.Exit(0);
        }

        async Task Execute() {
            var interval = Convert.ToInt32(ConfigurationManager.AppSettings["interval"]);
            var logFolder = ConfigurationManager.AppSettings["logFolder"];
            var errorFolder = ConfigurationManager.AppSettings["errorFolder"];
            string logFileName = DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day + ".log";
            string logFilePath = logFolder + @"\" + logFileName;
            string errorFilePath = errorFolder + @"\" + logFileName;

            while (true) {
                Console.WriteLine($"{DateTime.Now} - Start Job");
                using (StreamWriter sw = File.AppendText(logFilePath)) {
                    sw.WriteLine($"{DateTime.Now} - Start Job");
                }
                RunJob(logFilePath, errorFilePath);
                Console.WriteLine($"{DateTime.Now} - End Job");
                using (StreamWriter sw = File.AppendText(logFilePath)) {
                    sw.WriteLine($"{DateTime.Now} - End Job");
                }

                // 12:00:00 -> 12:01:40 -> 
                var timeToWait = interval - (DateTime.Now.Second % interval);
                //Thread.Sleep(timeToWait * 1000);
                await Task.Delay(timeToWait * 1000);
            }
        }

        public void RunJob(string logFilePath, string errorFilePath) {

            var script = ConfigurationManager.AppSettings["script"];
            try {
                var p = Process.Start(new ProcessStartInfo {
                    FileName = script,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    WorkingDirectory = ConfigurationManager.AppSettings["workingDirectory"]
                });


                using (StreamWriter sw = File.AppendText(logFilePath)) {
                    while (!p.StandardOutput.EndOfStream) {
                        var line = p.StandardOutput.ReadLine();
                        sw.WriteLine(line);
                        Console.WriteLine(line);
                    }
                }

                string stdError = p.StandardError.ReadToEnd();
                if (stdError != "") {
                    using (StreamWriter sw = File.AppendText(errorFilePath)) {
                        sw.Write(stdError);
                    }
                    Console.Write("standard error: {0}", stdError);
                }
            } catch (Exception e) {
                using (StreamWriter sw = File.AppendText(errorFilePath)) {
                    sw.WriteLine(e.Message);
                }
                Console.WriteLine(e.Message);
            }
        }

        async Task ExecuteSimpleJob (string jobName, int interval) {
            while (true) {
                Console.WriteLine($"{DateTime.Now} - Start Job: {jobName}");
                //using (StreamWriter sw = File.AppendText(logFilePath)) {
                //    sw.WriteLine($"{DateTime.Now} - Start Job");
                //}
                RunSimpleJob(jobName);
                Console.WriteLine($"{DateTime.Now} - End Job: {jobName}");
                //using (StreamWriter sw = File.AppendText(logFilePath)) {
                //    sw.WriteLine($"{DateTime.Now} - End Job");
                //}

                // 12:00:00 -> 12:01:40 -> 
                var timeToWait = interval - (DateTime.Now.Second % interval);
                //Thread.Sleep(timeToWait * 1000);
                await Task.Delay(timeToWait * 1000);
            }
        }

        public void RunSimpleJob(string jobName) {
            var p = Process.Start(new ProcessStartInfo {
                FileName = @"C:\Users\cegu5268\source\repos\winApp\jobs\" + jobName,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                WorkingDirectory = ConfigurationManager.AppSettings["workingDirectory"]
            });
            string stdOutput = p.StandardOutput.ReadToEnd();
            Console.WriteLine(stdOutput);
            string stdError = p.StandardError.ReadToEnd();
            if (stdError != "") {
                //using (StreamWriter sw = File.AppendText(errorFilePath)) {
                //    sw.Write(stdError);
                //}
                Console.Write("standard error: {0}", stdError);
            }
        }
    }

    internal static class ConfigureService {
        internal static void Configure() {
            HostFactory.Run(configure => {
                configure.Service<MyService>(service => {
                    service.ConstructUsing(s => new MyService());
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                //Setup Account that window service use to run.  
                configure.RunAsLocalSystem();
                configure.SetServiceName(ConfigurationManager.AppSettings["serviceName"]);
                configure.SetDisplayName(ConfigurationManager.AppSettings["serviceDisplayName"]);
                configure.SetDescription(ConfigurationManager.AppSettings["serviceDescription"]);
            });
        }
    }
}
